import pybgpstream
from pytricia import PyTricia
from collections import defaultdict

def ips(pyt):
    lsps = []
    for pref in pyt:
        if pyt.parent(pref) is None:
            lsps.append(pref)

    if len(lsps) == 0: return 0

    v6 = ':' in lsps[0]
    maxcidr = (32, 128)[v6]
    addr_cnt = 0
    for pref in lsps:
        addr_cnt += 2**(maxcidr - int(pref.split('/')[1]))
    return addr_cnt

def retrieve_data(day):
    kwargs = {"from_time":    day+' 00:00:00 UTC',
            "until_time":    day+' 00:00:00 UTC',
            "projects":     ["routeviews", "ris"],
            "record_type":   "ribs"}

    stream = pybgpstream.BGPStream(**kwargs)
    stream.set_data_interface_option("broker", "cache-dir", "../../data/bgpstream_cache/")
    for rec in stream.records():
        for elem in rec:
            yield elem

DATE='2021-07-01'
feed_routes_v4 = defaultdict(PyTricia)
feed_routes_v6 = defaultdict(lambda: PyTricia(128))
obs_routes_v4 = defaultdict(PyTricia)
obs_routes_v6 = defaultdict(lambda: PyTricia(128))
total_feeds = set()
for elem in retrieve_data(DATE):
    pfx = elem.fields["prefix"]
    v6 = ':' in pfx
    feed = elem.project + '-' + elem.collector.replace('-', '') + '-' + str(elem.peer_asn) + '-' + (elem.peer_address) + '-' + ('v4', 'v6')[v6]
    total_feeds.add(feed)
    feed_routes = (feed_routes_v4, feed_routes_v6)[v6]
    obs_routes = (obs_routes_v4, obs_routes_v6)[v6]
    for ASN in elem.fields["as-path"].split(" "):
        obs_routes[ASN].insert(pfx, '')
    feed_routes[feed].insert(pfx, '')

MAX_PREFS_v4 = 0
MAX_PREFS_v6 = 0
MAX_ADDRS_v4 = 0
MAX_ADDRS_v6 = 0

for feed in total_feeds:
    ASN = feed.split('-')[2]

    curr = len(feed_routes_v4[feed])
    if curr > MAX_PREFS_v4:
        MAX_PREFS_v4 = curr

    curr = len(feed_routes_v6[feed])
    if curr > MAX_PREFS_v6:
        MAX_PREFS_v6 = curr

    curr = ips(feed_routes_v4[feed])
    if curr > MAX_ADDRS_v4:
        MAX_ADDRS_v4 = curr

    curr = ips(feed_routes_v6[feed])
    if curr > MAX_ADDRS_v6:
        MAX_ADDRS_v6 = curr

columns = ['feed']
columns.append("feed_pref_cnt_v4")
columns.append("feed_pref_cnt_v6")
columns.append("feed_pref_addrs_v4")
columns.append("feed_pref_addrs_v6")
columns.append("obs_pref_cnt_v4")
columns.append("obs_pref_cnt_v6")
columns.append("obs_pref_addrs_v4")
columns.append("obs_pref_addrs_v6")
columns.append("perc_feed_prefs_v4")
columns.append("perc_feed_prefs_v6")
columns.append("perc_feed_addrs_v4")
columns.append("perc_feed_addrs_v6")
print('|'.join(columns))

for feed in total_feeds:
    ASN = feed.split('-')[2]
    feed_pref_cnt_v4 = len(feed_routes_v4[feed])
    feed_pref_cnt_v6 = len(feed_routes_v6[feed])

    feed_pref_addrs_v4 = ips(feed_routes_v4[feed])
    feed_pref_addrs_v6 = ips(feed_routes_v6[feed])

    obs_pref_cnt_v4 = len(obs_routes_v4[ASN])
    obs_pref_cnt_v6 = len(obs_routes_v6[ASN])

    obs_pref_addrs_v4 = ips(obs_routes_v4[ASN])
    obs_pref_addrs_v6 = ips(obs_routes_v6[ASN])

    perc_feed_prefs_v4 = float(feed_pref_cnt_v4)/MAX_PREFS_v4
    perc_feed_prefs_v6 = float(feed_pref_cnt_v6)/MAX_PREFS_v6

    perc_feed_addrs_v4 = float(feed_pref_addrs_v4)/MAX_ADDRS_v4
    perc_feed_addrs_v6 = float(feed_pref_addrs_v6)/MAX_ADDRS_v6

    fields = [feed]
    fields.append(str(feed_pref_cnt_v4))
    fields.append(str(feed_pref_cnt_v6))
    fields.append(str(feed_pref_addrs_v4))
    fields.append(str(feed_pref_addrs_v6))
    fields.append(str(obs_pref_cnt_v4))
    fields.append(str(obs_pref_cnt_v6))
    fields.append(str(obs_pref_addrs_v4))
    fields.append(str(obs_pref_addrs_v6))
    fields.append(str(perc_feed_prefs_v4))
    fields.append(str(perc_feed_prefs_v6))
    fields.append(str(perc_feed_addrs_v4))
    fields.append(str(perc_feed_addrs_v6))
    print("|".join(fields))
