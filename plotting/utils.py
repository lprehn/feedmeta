import matplotlib.pyplot as plt 
import numpy as np
from matplotlib.colors import LinearSegmentedColormap

LINE_STYLES = ['-', '--', '-.', ':']
MARKER_STYLES_LIMITED = ['o', 's', 'D', '>', 'x']
MARKER_STYLES_EXTENDED = ['.', 'o', 's', 'D', 'h', '^', '>', 'v', '<', '*', 'x', '+']

def get_color_palette(count = 4):
	if count >= 8:
		return ['#003f5c','#005b78','#007788','#009488','#00af7a','#55c760','#a8db40','#fde725']
	if count == 7:
		return ['#003f5c','#005f7c','#00818a','#00a183','#31bf69','#9bd845','#fde725']
	if count == 6: 
		return ['#003f5c','#006680','#008e89','#00b475','#88d44d','#fde725']
	if count == 5:
		return ['#003f5c','#007085','#00a183','#6bcc58','#fde725']
	if count == 4:
		return ['#003f5c','#00818a','#31bf69','#fde725']
	if count <= 3:
		return ['#003f5c','#00a183','#fde725']

def get_color_map(reverse = False ):
	colors = get_color_palette(count = 8)
	if reverse: colors = colors[::-1]
	hexcols = []
	for color in colors:
		r, g, b = tuple(int(color.lstrip('#')[i:i+2], 16) for i in (0, 2, 4))
		hexcols.append((r/256.0, g/256.0, b/256.0))
	return LinearSegmentedColormap.from_list('Blueridis', hexcols, N=256)



def get_color_palette_contrast_order(count = 4):

	colors = get_color_palette(count)

	# 8 values with at least 4 contrast between neighbors: 4, 0, 5, 1, 6, 2, 7, 3
	if count >= 8:
		return [colors[i] for i in [4, 0, 5, 1, 6, 2, 7, 3]]

	# 7 values with at least 3 contrast between neighbors: 5, 2, 6, 3, 0, 4, 1
	if count == 7:
		return [colors[i] for i in [5, 2, 6, 3, 0, 4, 1]]

	# 6 values with at least 3 contrast between neighbors: 2, 5, 1, 4, 0, 3
	if count == 6:
		return [colors[i] for i in [2, 5, 1, 4, 0, 3]]

	# 5 values with at least 2 contrast between neighbors: 2, 4, 0, 3, 1
	if count == 5:
		return [colors[i] for i in [2, 4, 0, 3, 1]]

	# 4 values with at least 2 contrast between neighbors: 1, 3, 0, 2
	if count == 4:
		return [colors[i] for i in [1, 3, 0, 2]]

	# 3 values with at least 1 contrast between neighbors: 1, 2, 3
	if count <= 3:
		return [colors[i] for i in [0,1,2]]

def get_3_column_figure():
	width = 9.6
	figure = plt.figure(figsize = (width/3.0, (width/3.0)))
	return figure

def get_2_column_figure():
	width = 9.6
	figure = plt.figure(figsize = (width/2.0, (width/3.0)))
	return figure


def to_max_diff_order(array):
	'''
	the arrary is assumed to be in visually-sorted order,
	it will afterwards be shuffled to present a max-difference
	order
	'''
	N = len(array)
	M = int(N/2)
	new_order = []
	even = N%2 == 0
	for i in range(M):
		new_order.extend([array[i], array[M+i+(1,0)[even]]])
	if not even: new_order.append(array[M])
	return new_order


def ecdf(data):
	x = sorted(data)
	n = len(x)
	y = np.arange(1, n+1) / n
	return(x,y)

def get_N_colors(N, maxdiff = True):
	CMAP = get_color_map()
	offset = 1.0/float((N-1))
	colors = [CMAP(i*offset) for i in range(N)]
	if maxdiff:
		return to_max_diff_order(colors)
	return colors

def get_N_curve_styles(N, maxdiff = True):
    global LINE_STYLES
    global MARKER_STYLES_EXTENDED
    global MARKER_STYLES_LIMITED

    if N > 5:	markers =  MARKER_STYLES_EXTENDED
    else: 		markers = MARKER_STYLES_LIMITED
    colors = get_N_colors(N, maxdiff = maxdiff)
    lines = LINE_STYLES
    if maxdiff:
    	lines = to_max_diff_order(lines)
    	markers = to_max_diff_order(markers)

    styles = []
    L, M = len(lines), len(markers)
    for i in range(N):
    	style = {	'marker': 	markers[i%M],
    				'ls':		lines[i%L],
    				'color':	colors[i]}
    	styles.append(style)
    return styles 

print(to_max_diff_order([1,2,3]))	







