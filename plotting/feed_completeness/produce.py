import sys
sys.path.insert(1, '../')
import utils 
import pandas as pd 
import matplotlib.pyplot as plt
from collections import defaultdict

FILE = '../../results/feed_completeness/completeness_results_updated.csv'
PLOT_PATH = '../../plots/feed_completeness/'
df = pd.read_csv(FILE, sep = '|')   
columns = list(df.columns)
columns4 = [c for c in columns if 'v4' in c]
columns6 = [c for c in columns if 'v6' in c]
print(columns4)
print(columns6)

grid_kwargs = {"linewidth": .5,
				"alpha": .5,
				"color": "grey",
				"zorder": 1}


def get_base_fig():
	fig = utils.get_3_column_figure()
	ax = plt.gca()
	ax.set_ylim(-0.1, 1.1)
	ax.set_xlim(-0.1, 1.1)
	ax.axhline(0.5)
	ax.axvline(0.5)
	for i in range(11):
		ax.axhline(i*0.1, **grid_kwargs)
		ax.axvline(i*0.1, **grid_kwargs)

	ax.set_xlabel('size of provided routing table')
	ax.set_ylabel('completeness of provided routing table')
	return fig, ax

def plot_2d_map(df, base, proto, ptype, grid_kwargs):
	fig, ax = get_base_fig()
	for index, row in df.iterrows():
		# check if feed is v4/v6:
		if proto == 'v4' and row['feed_pref_cnt_v4'] < 1.0: continue
		if proto == 'v6' and row['feed_pref_cnt_v6'] < 1.0: continue

		# get partial vs fullfeed
		x = row['perc_feed_'+ptype+'_'+proto]
		tail = ('cnt', 'addrs')[ptype == 'addrs']+'_'+proto

		# remember: pandas parses very long ints as strings, float conversion needed ...
		fed = float(row['feed_pref_'+tail])
		obs = max(float(row['obs_pref_'+tail]), fed)
		y = fed/obs
		color, marker = 'blue', 'x'
		if 'routeviews' in row['feed']:
			if base == 'rr': continue
			color, marker = 'red', 'o'
		else:
			if base == 'rv': continue
		ax.scatter(x,y, zorder = 10, color = color, alpha = 0.5, marker = marker)
	plt.tight_layout(pad = 0)
	FILE=PLOT_PATH+'overview/%s/vtypes_20210701_%s_%s.pdf' %(base, ptype, proto)
	plt.savefig(FILE)

def plot_per_mutli_2d_map(df, proto, ptype, grid_kwargs):
	data = defaultdict(list)
	ptype_adjusted = ('cnt', 'addrs')[ptype == 'addrs']
	max_val = 0.0
	for index, row in df.iterrows():
		try:
			max_val = max(max_val, float(row['feed_pref_'+ptype_adjusted+'_'+proto]))
		except:
			print(row)
			continue
		if proto == 'v4' and row['feed_pref_cnt_v4'] < 1.0: continue
		if proto == 'v6' and row['feed_pref_cnt_v6'] < 1.0: continue
		data[row['feed'].split('-')[2]].append(row)

	for asn in data:
		dps = data.get(asn)
		if len(dps) < 2: continue

		fig, ax = get_base_fig()
		for row in dps:
			# get partial vs fullfeed
			x = row['perc_feed_'+ptype+'_'+proto]
			tail = ('cnt', 'addrs')[ptype == 'addrs']+'_'+proto

			# remember: pandas parses very long ints as strings, float conversion needed ...
			fed = float(row['feed_pref_'+tail])
			obs = max(float(row['obs_pref_'+tail]), fed)
			y = fed/obs
			color, marker = 'blue', 'x'
			if 'routeviews' in row['feed']:
				color, marker = 'red', 'o'
			ax.scatter(x,y, zorder = 10, color = color, alpha = 0.5, marker = marker)
			ax.text(x+(0.05, -0.05)[x > 0.5],y,"(%f,%f) %s" % (x,y,row['feed']), ha = ('left', 'right')[x > 0.5], va = 'center', fontsize = 4)
		x = row['perc_peer_'+ptype+'_'+proto]
		fed = int(x*max_val)
		obs = max(float(row['obs_pref_'+tail]), fed)
		y = fed/obs
		ax.scatter(x,y, zorder = 20, edgecolor = 'green', alpha = 1, marker = 's', facecolor='none')
		plt.tight_layout(pad = 0)
		FILE=PLOT_PATH+'multifeeds/%s/%s/20210701_%s_%s_%s_%s.pdf' %(proto, ptype, str(len(dps)).zfill(2),  str(asn), ptype, proto)
		plt.savefig(FILE)
		plt.close()

def plot_ecdf(df, proto, ptype, grid_kwargs):
	
	tail = ('cnt', 'addrs')[ptype == 'addrs']+'_'+proto
	values = defaultdict(lambda: defaultdict(list))
	colors = utils.get_color_palette_contrast_order(3)
	for index, row in df.iterrows():
		# check if feed is v4/v6:
		if proto == 'v4' and row['feed_pref_cnt_v4'] < 1.0: continue
		if proto == 'v6' and row['feed_pref_cnt_v6'] < 1.0: continue

		# get partial vs fullfeed
		x = row['perc_feed_'+ptype+'_'+proto]
		# remember: pandas parses very long ints as strings, float conversion needed ...
		fed = float(row['feed_pref_'+tail])
		obs = max(float(row['obs_pref_'+tail]), fed)
		y = fed/obs

		if 'routeviews' in row['feed']:
			values['size']['rov'].append(x)
			values['comp']['rov'].append(y)
		else:
			values['size']['ris'].append(x)
			values['comp']['ris'].append(y)
		values['size']['all'].append(x)
		values['comp']['all'].append(y)

	for dim in ['size', 'comp']:
		fig = utils.get_3_column_figure()
		ax = plt.gca()
		ax.set_ylim(-0.1, 1.1)
		ax.set_xlim(-0.1, 1.1)
		for i in range(11):
			ax.axhline(i*0.1, **grid_kwargs)
			ax.axvline(i*0.1, **grid_kwargs)
		for i, proj in enumerate(['ris', 'rov', 'all']):
			vals = values[dim][proj]
			x,y = utils.ecdf(vals)
			ax.plot(x,y, label = proj, color = colors[i])
		ax.legend(loc = ('lower right', 'upper left')[dim == 'comp'])
		ax.set_ylabel('ECDF(x)')
		ax.set_xlabel(('RT size', 'Completeness')[dim == 'comp'])
		plt.tight_layout(pad = 0)
		FILE=PLOT_PATH+'ecdfs/%s_20210701_%s_%s.pdf' %(dim, ptype, proto)
		plt.savefig(FILE)

for proto in ['v4', 'v6']:
	for ptype in ['prefs', 'addrs']:
		#for base in ['rv', 'rr', 'all']:
		#	plot_2d_map(df, base, proto, ptype, grid_kwargs)
		#plot_ecdf(df, proto, ptype, grid_kwargs)
		plot_per_mutli_2d_map(df, proto, ptype, grid_kwargs)


# TODOS:
# add ecdfs for completeness as well as feed size
# add separate plots for ASes with mutliple feeds 